import React from 'react';
import { StyleSheet, Text, View, ScrollView, Navigator } from 'react-native';
import { Container, Content, Spinner, Header } from 'native-base';
import MapLayout from './ui/MapLayout';
import GroceryScreen from './ui/GroceryScreen';
import CheckoutScreen from './ui/CheckoutScreen';
import GroceryDirectory from './ui/GroceryDirectory';
import OrderScreen from './ui/OrderScreen';
import LogInLayout from './ui/LogInLayout';
import { StackNavigator } from 'react-navigation';

const AppNavigator = StackNavigator({
    Grocery: {
      screen: GroceryScreen
    },
    Checkout: {
      screen: CheckoutScreen
    },
    Order: {
      screen: OrderScreen
    }
  },
  {
    headerMode: 'none'
  }
);

export default class App extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      fontLoaded: false,
    }
  }

  // Imports custom fonts
  async componentDidMount(){
    await Expo.Font.loadAsync({
      'Roboto': require('./assets/fonts/Roboto/Roboto-Regular.ttf'),
      'Roboto_medium': require('./assets/fonts/Roboto/Roboto-Medium.ttf'),
    });

    this.setState({fontLoaded: true});
  }

  render() {
    if(this.state.fontLoaded){

      // Finished loading
      return (

        <LogInLayout />
      );
    }else{

      // While loading
      return(
        <Container style={{flex: 1}}>
            <View style={styles.loadingContainer}>
              <Spinner color='blue' />
            </View>
        </Container>
      );
    }
  }
}

const styles = StyleSheet.create({
  loadingContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
