import React from 'react';
// Native Base Components
import { StyleSheet, View, FlatList } from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Input, Item, Text, Drawer } from 'native-base';
import ItemProduct from './item/ItemProduct.js';
import GroceryDirectory from './GroceryDirectory.js';
import Categories from './CategoriesDirectory.js';
import MenuScreen from './MenuScreen.js';

export default class GroceryScreen extends React.Component {

  openSlideMenu () {
    this.drawer._root.open();
  }

  CloseSlideMenu () {
    this.drawer._root.close();
  }

  ChangeCategorySelected(category){

    if(this.categorySelected === category)
      return;

    this.categorySelected = category;

    this.setState({items: this.SearchFilter(GroceryDirectory.items, this.searchText, this.categorySelected)});
  }

  ChangeText(search){

    this.searchText = search;

    this.setState({items: this.SearchFilter(GroceryDirectory.items, this.searchText, this.categorySelected)});
  }

  renderItem(itemContainer){
    const item = itemContainer['item'];
    return <ItemProduct name={item.name} title={item.title} price={item.price} description={item.description} category={item.category} key={item.id} id={item.id} />;
  }

  constructor(props){
    super(props);

    this.state = {
      items: GroceryDirectory.items,
    };

    this.navigation = this.props.navigation;
    this.items = GroceryDirectory.items;
    this.categorySelected = 'Todos';
    this.searchText = '';

    this.renderItem = this.renderItem.bind(this);
    this.openSlideMenu = this.openSlideMenu.bind(this);
    this.CloseSlideMenu = this.CloseSlideMenu.bind(this);
    this.ChangeCategorySelected = this.ChangeCategorySelected.bind(this);
    this.SearchFilter = this.SearchFilter.bind(this);
    this.goToCheckoutScreen = this.goToCheckoutScreen.bind(this);
  }

  goToCheckoutScreen(){

    let buyingItems = false;

    for(let i = 0; i < GroceryDirectory.items.length; i++){
      if(GroceryDirectory.items[i].quantity > 0){
        buyingItems = true;
        break;
      }
    }


    if(buyingItems){
      this.navigation.navigate('Checkout');
    }else{
      alert('Porfavor seleccione al menos un producto para continuar.');
    }

  }

  render() {
    const MenuComponent = (
      <MenuScreen CloseSlideMenu={this.CloseSlideMenu} SelectCategory={this.ChangeCategorySelected} />
    );

    return (

      <Drawer
        ref={(ref) => this.drawer = ref}
        content={MenuComponent}
        type='overlay'
        openDrawerOffset={180}
        onClose={() => this.CloseSlideMenu()}>

        <Container>
          <Header rounded style={{backgroundColor: '#00BCD4', height: 65}}>
            <Left style={{height: '100%', justifyContent: 'flex-end', flex: 1}}>
              <Button transparent style={{width: '100%', justifyContent: 'center'}} onPress={() => this.openSlideMenu()}>
                <Icon name='menu' />
              </Button>
            </Left>
            {/* Search Bar */}
            <Item style={{backgroundColor: '#E8EAF6', flex: 7, height: 35, alignSelf: 'flex-end', marginBottom: 5}}>
              <Icon name='ios-search' style={{marginLeft: 5}} />
              <Input placeholder='Search' style={{marginLeft: -7, paddingRight: 7}}
                  onChangeText={(text) => this.ChangeText(text)} />
            </Item>
          </Header>
          {/* Container for the Items */}
            <FlatList
              data={this.state.items}
              renderItem={this.renderItem}
              keyExtractor={item => item.id} />

          <Footer style={{backgroundColor: '#00A823'}}>
            <Button full iconRight transparent style={{height: '100%', width: '100%'}} onPress={() => this.goToCheckoutScreen()}>
              <Text style={{fontSize: 27, color: '#EEEEEE'}}>Siguiente</Text>
              <Icon name="cart" style={{fontSize: 40, color: '#EEEEEE'}} />
            </Button>
          </Footer>
        </Container>

      </Drawer>
    );
  }

  SearchFilter(tempItemsObject, filterText, filterCategory){

    // Remove all whitespace at the edges.
    filterText = filterText.toLowerCase().trim();

    // Remove dots and withespace in search.
    while(filterText.includes('.')){
      filterText = filterText.replace(' ', '.');
    }

    while(filterText.includes('  ')){
      filterText = filterText.replace('  ', ' ');
    }

    // Show only Items who fulfill search and category filter.
    return tempItemsObject.filter((item) => {
      if((item['title'].toLowerCase().includes(filterText) ||
          item['description'].toLowerCase().includes(filterText) ||
            item['category'].toLowerCase().includes(filterText)) && (filterCategory === 'Todos' || item['category'] === filterCategory)){
        return item;
      }
    });
  }
}

const styles = StyleSheet.create({
  itemsContainer: {
    flex: 1,
    width: '100%',
    flexDirection: 'column',
    alignSelf: 'center',
  },
  icon: {
    width: 24,
    height: 24,
  },
});
