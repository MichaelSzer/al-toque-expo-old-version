// Import all the itemImages
const Images = {
  'apple-fruit': require('../assets/images/apple-fruit.png'),
  'lays-classic': require('../assets/images/lays-classic.png'),
  'pepsi': require('../assets/images/pepsi.png'),
  'leche-entera': require('../assets/images/milk-leche-entera.png'),
  'leche-ultra-pasteurizada': require('../assets/images/milk-ultra-pasteurizada.jpg'),
  'BC-Manzana': require('../assets/images/BC-Manzana.png'),
  'BC-Naranja': require('../assets/images/BC-Naranja.png'),
  'Pep-palitos-salados': require('../assets/images/Pep-palitos-salados.jpg'),
  'Pep-reduitas': require('../assets/images/Pep-reduitas.jpg'),
  'Rex-original': require('../assets/images/Rex-original.jpg'),
  'Rex-kesitas': require('../assets/images/Rex-kesitas.jpg'),
  'Alfajor-terrabusi-chocolate-negro': require('../assets/images/Alfajor-terrabusi-chocolate-negro.jpg'),
  'Alfajor-terrabusi-chocolate-blanco': require('../assets/images/Alfajor-terrabusi-chocolate-blanco.jpg'),
  'Alfajor-ser': require('../assets/images/Alfajor-ser.jpg'),
  'Alfajor-jorgito-chocolate-mini-6-unidades': require('../assets/images/Alfajor-jorgito-chocolate-mini-6-unidades.png'),
  'Aceite-girasol-natura-1.5-litros': require('../assets/images/Aceite-girasol-natura-1.5-litros.jpg'),
  'Aceite-girasol-natura-900-gramos': require('../assets/images/Aceite-girasol-natura-900-gramos.jpg'),
  'Aceite-oliva-cocinero-virgen-1-litro': require('../assets/images/Aceite-oliva-cocinero-virgen-1-litro.jpg'),
  'Chipa-Lucceti': require('../assets/images/Chipa-Lucceti.png'),
  'Banana-fruit': require('../assets/images/Banana-fruit.jpg'),
  'Naranja-fruit': require('../assets/images/Naranja-fruit.jpg'),
  'Lechuga-vegetable': require('../assets/images/Lechuga-vegetable.jpg'),
}

module.exports = Images;
