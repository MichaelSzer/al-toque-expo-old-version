import RNFirebase from 'react-native-firebase';

export function initializeApp() {

  // Set all the keys to connect with Firebase and start connection
  return RNFirebase.initializeApp({
    apiKey: "AIzaSyCilPKnBcZN_V_C7fDJTaH3FIEJUNb_8mc",
    authDomain: "al-toque-5ba3b.firebaseapp.com",
    databaseURL: "https://al-toque-5ba3b.firebaseio.com",
    projectId: "al-toque-5ba3b",
    storageBucket: "al-toque-5ba3b.appspot.com",
    messagingSenderId: "98097847925"
  });
}

export async function singup(firebaseSession, email, pass) {

  try {
    await firebaseSession.auth()
                      .createUserWithEmailAndPassword(email, pass);

    console.log("Account created.");

    // Navigate to the Home page
  } catch (error) {
    console.log(error.toString());
  }
}

export async function singin(firebaseSession, email, pass) {

  await firebaseSession.auth().signInWithEmailAndPassword(email, password)
    .then((userData) => {

      // You need to store userData
      console.log(JSON.stringify(userData));

      try {
        AsyncStorage.setItem('userData', JSON.stringify(userData));
        AsyncStorage.setItem('userLoggedIn', true);
      } catch (error) {
        // Error saving data
        console.log(error);
      }
    }
  ).catch((error) =>
      {
            this.setState({
              loading: false
            });
      alert('Login Failed. Please try again'+error);
  });
}
