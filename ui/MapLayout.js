import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import MapView from 'react-native-maps';

export default class MapLayout extends React.Component {
  constructor(props){
    super(props);
    this.state = {
        // You determined the zoom level with longitude/latitudeDelta
        initialPositionCoordinates: {
          latitude: -34.603684,
          latitudeDelta: 0.00922*1.2,
          longitude: -58.381559,
          longitudeDelta: 0.00421*1.2,
        },
    }
  }

  render() {

    return (

      <View style ={styles.container}>
        <MapView
          ref={(component) => this.mapView = component}
          style={styles.map}
          initialRegion={this.state.initialPositionCoordinates}
          showsUserLocation={true}
          showsCompass={false}
          showsPointsOfInterest={false}
          showsBuildings={false}
          loadingEnabled={true} >
          <MapView.Marker
            title="house"
            coordinate={{
              latitude: -34.527416,
              longitude: -58.480982,
            }}
            image={source=require('../assets/images/michael.jpeg')} />
        </MapView>
      </View>
    );
  }

  componentDidMount(){

    navigator.geolocation.getCurrentPosition( (position) => {

      var tempInitialPosition = JSON.parse(JSON.stringify(this.state.initialPositionCoordinates));

      tempInitialPosition.latitude = position.coords.latitude;
      tempInitialPosition.longitude = position.coords.longitude;

      this.setState({initialPositionCoordinates: tempInitialPosition});

      var mapRef = this.mapView;

      mapRef.animateToRegion({
        ...this.state.initialPositionCoordinates,
      }, 2000);
      // Duration in miliseconds

    }, (error) => alert(error.message), {enableHighAccuracy: true} );

  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },

  map: {
    ...StyleSheet.absoluteFillObject,
    flex: 1,
  },
});
