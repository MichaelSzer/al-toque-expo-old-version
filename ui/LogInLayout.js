import React, { Component } from 'react';
import { Header,Container,Title, Content, List, ListItem, InputGroup, Input, Picker, Button, Body, Spinner } from 'native-base';
import { StyleSheet, View, Text, Image } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
// import { GoogleSignin } from 'react-native-google-signin';
import { LoginManager } from 'react-native-fbsdk';
import { singin, initializeApp } from './FirebaseConnection.js';

export default class LoginLayout extends Component {
  constructor(props){
    super(props);

    this.state = {
      loading: false,
      email: '',
      password: ''
    }

    this.facebookCallback = this.facebookCallback.bind(this);
    this.signInGoogle = this.signInGoogle.bind(this);
    this.signInFacebook = this.signInFacebook.bind(this);
    this.signInFirebase = this.signInFirebase.bind(this);
  }

  signInFacebook() {

    // Attempt a login usSing the Facebook login dialog asking for default permissions.
    /*LoginManager.logInWithReadPermissions(['public_profile']).then(
      function(result) {
        if (result.isCancelled) {
          alert('Login cancelled.');
        } else {
          alert('Login success with permissions: ' + result.grantedPermissions.toString());
        }
      },
      function(error) {
        alert('Login fail with error: ' + error);
      }
    );*/
  }

  facebookCallback(error, result) {
    if (error) {
      alert("Login failed with error: " + result.error);
    } else if (result.isCancelled) {
      alert("Login was cancelled");it
    } else {
      alert("Login was successful with permissions: " + result.grantedPermissions)
    }
  }

  async signInFirebase(email, password) {

    if(!email || !password){
      alert('Porfavor ingrese su correo/contraseña.');
      return;
    }

    this.setState({loading: true});

    if(!this.firebaseSession){

      this.firebaseSession = initializeApp();
    }

    console.log(this.firebaseSession);

    singup(this.firebaseSession, email, password);

    this.setState({ loading: false });
  }

  signInGoogle() {

    // Check if Player Services is installed
    /*GoogleSignin.hasPlayerServices({autoResolve: true}).then(() => {

    }).catch((error) => {
      console.log("Play services error", error.code, error.message);
    });

    // Google sign in
    GoogleSignin.signIn()
    .then((user) => {
      console.log(user);
    })
    .catch((error) => {
      console.log('WRONG SIGNIN', err);
    });*/
  }

  render() {

    // Stylesheet is not compatible with <Button> so you have to make it plain text.
    const styles_primaryButton = StyleSheet.flatten(styles.primaryButton);

    // The content of the screen should be inputs for a username, password and submit button.
    // If we are loading then we display an ActivityIndicator.
    const content = this.state.loading ?
    <View style={styles.body}>
      <Spinner color='blue' />
    </View> :

    <Content contentContainerStyle={{flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
      <List style={{width: '100%'}}>
       <ListItem>
           <InputGroup>
           <Icon name="account" size={18} style={{ color: '#607D8B', marginLeft: 5 }} />
           <Input
            onChangeText={(text) => this.setState({email: text})}
            value={this.state.email}
            placeholder={"Email Address"} />
            </InputGroup>
      </ListItem>
      <ListItem>
          <InputGroup>
            <Icon name="lock-open" size={17} style={{ color: '#607D8B', marginLeft: 5 }} />
            <Input
              onChangeText={(text) => this.setState({password: text})}
              value={this.state.password}
              secureTextEntry={true}
              placeholder={"Password"}
              style={{color: '#607D8B'}} />
          </InputGroup>
     </ListItem>
    </List>

    {/* Standard Log in */}
    <View style={styles.logInButtonsView}>
      <Button onPress={() => this.signInFirebase(this.state.email, this.state.password)} style={styles_primaryButton}>
        <Text style={{color: '#FAFAFA', fontSize: 16}}>Iniciar sesión</Text>
      </Button>
      <Button style={styles_primaryButton}>
        <Text style={{color: '#FAFAFA', fontSize: 16}}>Registrarse</Text>
      </Button>
    </View>

    {/* Facebook Log in */}
    <Button bordered iconLeft onPress={this.signInFacebook} light style={{alignSelf: 'center', marginTop: 20, borderColor: '#3B5998', borderWidth: 1, width: 220}}>
      <Icon name='facebook-box' size={18} style={{color: '#3B5998', marginRight: 5}}/>
      <Text style={{color: '#3B5998', fontSize: 15}}>continuar con Facebook</Text>
    </Button>

    {/* Google Log in */}
    <Button bordered iconLeft light style={{alignSelf: 'center', justifyContent: 'flex-start', marginTop: 20, borderColor: '#EA4335', borderWidth: 1, width: 220}}>
      <Image source={require('../assets/icons/google.png')} style={{marginRight: 8, height: 18, width: 18}}/>
      <Text style={{color: '#EA4335', fontSize: 15, textAlign: 'left'}}>continuar con Google</Text>
    </Button>

  </Content>;

    // A simple UI with a toolbar, and content below it.
    return (
      <Container>
        <Header style={{backgroundColor: '#00BCD4', height: 65}}>
          <Body style={{height: '100%', alignItems: 'center', justifyContent: 'flex-end'}}>
            <Title style={{paddingBottom: 8, fontSize: 25}}>
              Entrar
            </Title>
          </Body>
        </Header>

        {content}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'stretch',
    flex: 1
  },
  body: {
    flex: 9,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    backgroundColor: '#F5FCFF',
  },
  primaryButton: {
    margin: 10,
    padding: 15,
    alignSelf:'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#00BCD4',
    width: 150
  },
  logInButtonsView: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
});
