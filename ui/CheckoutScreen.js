import React from 'react';
// Native Base Components
import { StyleSheet, View, FlatList, BackHandler } from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Input, ListItem, Text, Drawer } from 'native-base';
import ItemProduct from './item/ItemProduct.js';
import GroceryDirectory from './GroceryDirectory.js';
import Categories from './CategoriesDirectory.js';
import MenuScreen from './MenuScreen.js';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

class ListHeader extends React.Component {

  render(){
    return (
      <ListItem style={{backgroundColor: '#EEEEEE'}}>
        <Left>
          <Text style={{fontSize: 18}}>Producto</Text>
        </Left>
        <Body style={{flexDirection: 'row', flex: 1}}>
          <Text style={{alignSelf: 'center', fontSize: 18}}>Cantidad</Text>
          <Text style={{alignSelf: 'flex-end', fontSize: 18}}>Precio</Text>
        </Body>
      </ListItem>
    );
  }
}

export default class CheckoutScreen extends React.Component {

  static get defaultProps() {
    return {
      title: 'CheckoutScreen'
    };
  }

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "86%",
          backgroundColor: "#CED0CE",
          marginLeft: "14%"
        }}
      />
    );
  }

  makeMailBody() {

    let body = "";

    this.purchasedItems.map((item) => {
      body += item.title + ", " + item.description + ", " + item.quantity + ", $" + item.price + ".\n";
    });

    body += "Delivery: $" + this.deliveryCost + ".\nCosto Total: $" + this.totalCost + ".\n";

    return body;
  }

  async sendOrder () {

    var communicationer = require('react-native-communications');

    // Send Email
    await communicationer.email(['altoqueMichael@gmail.com'], null, null, 'ACA PONES TU DIRECCION Y NOMBRE', this.makeMailBody());

    // Change to the OrderScreen
    this.navigation.navigate('Order');
  }

  renderItem(itemContainer) {
    const item = itemContainer['item'];
    return (
      <ListItem itemDivider style={{backgroundColor: '#EEEEEE'}}>
        <Left>
          <Text>{item.title}</Text>
        </Left>
        <Body style={{flexDirection: 'row', flex: 1}}>
          <Text style={{flex: 1}}>{item.quantity}</Text>
          <Text style={{flex: 1}}>${item.price}</Text>
        </Body>
      </ListItem>
    );
  }

  constructor(props){
    super(props);

    this.itemsCost = 0;
    this.navigation = this.props.navigation;

    this.purchasedItems = GroceryDirectory.items.filter((item) => {
      if(item.quantity > 0){
        this.itemsCost += item.price*item.quantity;
        return item;
      }
    });

    this.deliveryCost = 60;
    this.totalCost = (this.deliveryCost + this.itemsCost).toFixed(2);

    this.sendOrder = this.sendOrder.bind(this);
  }

  render() {

    return (

      <Container>
        <Header style={{backgroundColor: '#00BCD4', height: 65}}>
          <Body style={{height: '100%', alignItems: 'center', justifyContent: 'flex-end'}}>
            <Title style={{paddingBottom: 8, fontSize: 25}}>
              Caja
            </Title>
          </Body>
        </Header>
        {/* Container for the Items */}
        <Content>
          {/* Each item has 46 height */}
          <View style={{height: 287, backgroundColor: '#EEEEEE'}}>
            <ListHeader />
            <FlatList
              data={this.purchasedItems}
              renderItem={this.renderItem}
              keyExtractor={item => item.id}
              ItemSeparatorComponent={this.renderSeparator} />
          </View>
          <View style={{flex: 1, width: '100%', flexDirection: 'column', justifyContent: 'center', alignSelf: 'center'}}>
            <View style={StyleSheet.flatten([styles.costContainer, {marginTop: 10}])}>
              <Text style={{fontSize: 21}}>Costo:</Text>
              <Text style={{fontSize: 19}}>${this.itemsCost.toFixed(2)}</Text>
            </View>
            <View style={StyleSheet.flatten([styles.costContainer, {marginTop: 8}])}>
              <Text style={{fontSize: 21}}>Delivery:</Text>
              <Text style={{fontSize: 19}}>${this.deliveryCost.toFixed(2)}</Text>
            </View>
            <View style={{ marginTop: 4, width: '50%', alignSelf: 'center', height: StyleSheet.hairlineWidth, backgroundColor: '#000000'}} />
            <View style={StyleSheet.flatten([styles.costContainer, {marginTop: 5}])}>
              <Text style={{fontSize: 21}}>Total:</Text>
              <Text style={{fontSize: 19}}>${this.totalCost}</Text>
            </View>
          </View>
          <View style={{flexDirection: 'column', justifyContent: 'flex-end', marginTop: 30 }}>
            <Text style={{alignSelf: 'flex-start'}}>Los precios pueden llegar a variar hasta un 10%</Text>
          </View>
        </Content>
        <Footer style={{backgroundColor: '#FFFFFF', height: 60, justifyContent: 'center'}}>
          <Button iconLeft transparent style={{flex: 1, height: '100%', justifyContent: 'center', alignItems: 'center', backgroundColor: '#00B81D'}} onPress={() => this.navigation.goBack(null)}>
            <Icon name="arrow-left" style={{fontSize: 28, color: '#EEEEEE'}} />
            <Text style={{fontSize: 16, color: '#EEEEEE'}}>Volver</Text>
          </Button>
          <Button iconRight transparent style={{flex: 2, height: '100%', justifyContent: 'center', alignItems: 'center', backgroundColor: '#00A823'}} onPress={() => this.sendOrder()}>
            <Text style={{fontSize: 24, color: '#EEEEEE'}}>Pedir</Text>
            <Icon name="cash-multiple" style={{fontSize: 33, color: '#EEEEEE'}} />
          </Button>
        </Footer>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  costContainer: {
    marginTop: 5,
    width: '50%',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  icon: {
    width: 24,
    height: 24,
  },
});
