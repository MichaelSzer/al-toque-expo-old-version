import React from 'react';
import GroceryScreen from './GroceryScreen';
import MenuScreen from './MenuScreen';
import ScrollView from 'react-native';


export default class GroceryMain extends React.Component {

  constructor (props, context) {
    super(props, context);

    this.state = {
      categorySelected: 'Todos',
    };

    this.OpenSlideMenu = this.OpenSlideMenu.bind(this);
    this.CloseSlideMenu = this.CloseSlideMenu.bind(this);
    this.ChangeCategorySelected = this.ChangeCategorySelected.bind(this);

    this.previousCategorySelected = 'Todos';
  }

  render () {
    const MenuComponent = (
      <MenuScreen CloseSlideMenu={this.CloseSlideMenu} SelectCategory={this.ChangeCategorySelected} />
    );

    return (
      <Drawer
        ref={(ref) => this.drawer = ref}
        content={MenuComponent}
        onClose={() => this.CloseSlideMenu()}>
        <GroceryScreen selectedCategory={this.state.categorySelected} OpenSlideMenu={this.OpenSlideMenu} />
      </Drawer>
    );
  }
}

const drawerStyles = {
  main: { backgroundColor: '#FFFFFF' },
}
