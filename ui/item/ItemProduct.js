import React, { Component } from 'react';
import { StyleSheet, View, Text, TextInput, Image, TouchableHighlight } from 'react-native';
import PropTypes from 'prop-types';
import { Button, Card, CardItem, Body } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import Categories from '../CategoriesDirectory.js';
import GroceryDirectory from '../GroceryDirectory.js';
import Images from '../Images.js';

export default class ItemProduct extends Component {
  constructor(props){
    super(props);

    this.state = {
        inputText: '0',
    }
  }

  render() {
    return (
      <View style={StyleSheet.flatten([styles.container, { backgroundColor: Categories.list['backgroundColor'][this.props.category] }])}>
        <View style={{width: 340}}>
          <Card>
              {/* Side with Image */}
              <View style={styles.itemContainer}>
                  <Image source={Images[this.props.name]} style={styles.itemImage} />
                  <View style={{width: '100%', flexDirection: 'row', justifyContent: 'flex-end'}}>
                    <View style={{flex: 1}}>
                      <Text style={styles.textTitle}>{this.props.title}</Text>
                      <Text style={styles.textDescription}>{this.props.description}</Text>
                    </View>
                    <Text style={StyleSheet.flatten([styles.textTitle, { marginRight: 5, alignSelf: 'flex-end' } ])}>${this.props.price}</Text>
                  </View>
              </View>
              {/* Side with Buttons and Input */}
              <View style={styles.interactiveContainer}>
                <View style={styles.buttonContainer}>
                  {/* If you dont use 'height: "100%"' the Button doesn't respect Parent Sizes 9 */}
                  <Button bordered dark style={{height: '100%'}} onPress={this.plusButtonPressed}>
                    <Icon size={20} name='plus' />
                  </Button>
                </View>

                <TextInput style={styles.textInput}
                  underlineColorAndroid='transparent'
                  textAlign="center"
                  value={this.state.inputText}
                  keyboardType="numeric"
                  maxLength={2}
                  editable={false}
                  onSubmitEditing={(event) => this.textHandler(event.nativeEvent.text)}
                  onChangeText={(text) => this.setState({inputText: text})} />

                <View style={StyleSheet.flatten([styles.buttonContainer, { marginTop: 5 }])}>
                  <Button bordered dark style={{height: '100%'}} onPress={this.minusButtonPressed}>
                    <Icon size={20} name='minus' />
                  </Button>
                </View>
              </View>
            </Card>
        </View>
      </View>
    );
  }

  /*textHandler = (text) => {

    if(text == '' || text == '-' ){
      text = '0';
    }

    this.setState({inputText: text});
  }*/

  plusButtonPressed = () => {

    const inputNumber = parseInt(this.state.inputText);

    let output = inputNumber + 1;

    if(output > 99){
      output = 99;
    }

    GroceryDirectory.items[this.props.id].quantity = output;
    this.setState({inputText: output.toString()});
  }

  minusButtonPressed = () => {

    const inputNumber = parseInt(this.state.inputText);

    let output = inputNumber - 1;

    if(output < 0){
      output = 0;
    }

    GroceryDirectory.items[this.props.id].quantity = output;
    this.setState({inputText: output.toString()});
  }
}

const styles = StyleSheet.create({
  container: {
    height: 200,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  itemContainer: {
    height: '100%',
    width: '80%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    backgroundColor: '#F5F5F5',
  },
  itemImage: {
    flex: 1,
    resizeMode: 'contain',
    backgroundColor: '#FFFFFF',
  },
  textTitle: {
    fontSize: 17,
    fontFamily: 'Roboto',
    alignSelf: 'flex-start',
    marginLeft: 5,
  },
  textDescription: {
    fontSize: 12,
    fontFamily: 'Roboto',
    alignSelf: 'flex-start',
    marginLeft: 5,
    marginBottom: 3,
  },
  interactiveContainer: {
    height: '100%',
    width: '20%',
    alignItems: 'center',
    flexDirection: 'column',
    marginTop: 10,
    paddingLeft: 3,
    backgroundColor: '#FAFAFA',
  },
  buttonContainer: {
    height: '28%',
  },
  textInput: {
    aspectRatio: 0.80,
    height: 40,
    fontSize: 30,
    fontFamily: 'Roboto',
    marginTop: 5,
  },
});

ItemProduct.ViewPropTypes = {
  title: PropTypes.string,
  name: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  category: PropTypes.string.isRequired,
};
ItemProduct.defaultProps = {
  title: '',
  name: null,
  description: null,
  category: null,
};
