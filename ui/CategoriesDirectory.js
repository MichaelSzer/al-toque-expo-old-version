class Categories{
}

Categories.list = {
  'name': [
    'Todos',
    'Aperitivos',
    'Bebidas',
    'Lácteos',
    'Frutas',
    'Verduras',
    'Aceites',
    'Cocina'
  ],
  'backgroundColor': {
    'Frutas': '#FFCDD2',
    'Verduras': '#C8E6C9',
    'Aperitivos': '#D1C4E9',
    'Bebidas': '#B3E5FC',
    'Lácteos': '#FFF9C4',
    'Aceites': '#FFF59D',
    'Cocina': '#F0F4C3',
  },
};

module.exports = Categories;
