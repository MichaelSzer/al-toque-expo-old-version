import React from 'react';
// Native Base Components
import { StyleSheet, View, Text, TouchableHighlight } from 'react-native';
import { Icon } from 'native-base';
import { List, ListItem } from 'react-native-elements';
import Categories from './CategoriesDirectory.js';

export default class MenuScreen extends React.Component {
  /*static navigationOptions = {
    drawerLabel: 'Notifications',
    drawerIcon: ({ tintColor }) => (
      <Icon
        name='menu'
        style={StyleSheet.flatten([styles.icon, {color: tintColor}])} />
    ),
  }*/

  render(){
    return(
      <TouchableHighlight onPress={() => this.props.CloseSlideMenu()} activeOpacity={1}>
        <View style={{backgroundColor: '#FFFFFF', height: '100%'}}>
          <Text style={styles.textCategorias}>{'Categorías:'}</Text>
          <List>
            {
              Categories.list.name.map((category, index) => (
                <ListItem title={category} titleStyle={styles.title} onPress={() => {

                    this.props.SelectCategory(category);
                    this.props.CloseSlideMenu();
                  }}
                  key={index}
                  containerStyle={{backgroundColor: Categories.list['backgroundColor'][category]}}
                  rightIcon={{name: 'chevron-right', color: '#212121'}}/> ))
            }
          </List>
        </View>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontFamily: 'Roboto',
    fontSize: 20,
  },
  textCategorias: {
    fontFamily: 'Roboto',
    fontSize: 24,
    alignSelf: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    marginTop: 22,
  },
  icon: {
    width: 24,
    height: 24,
  },
});
