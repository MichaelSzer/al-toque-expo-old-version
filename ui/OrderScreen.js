import React from 'react';
// Native Base Components
import { StyleSheet, View, FlatList, Image, BackHandler } from 'react-native';
import { Container, Header, Title, Content, Footer, Button, Left, Right, Body, Input, ListItem, Text, Drawer } from 'native-base';
import GroceryDirectory from './GroceryDirectory.js';
import { NavigationActions } from 'react-navigation';

export default class OrderScreen extends React.Component {

  static get defaultProps() {
    return {
      title: 'OrderScreen'
    };
  }

  shutdownOrderScreen(){

    // Stop the interval
    clearInterval(this.intervalCountdown);

    // Stop eventListener
    BackHandler.removeEventListener('hardwareBackPress', null);

    // Set all items quantity to 0
    GroceryDirectory.items.map((item) => {
      item.quantity = 0;
    });

    return this.props
               .navigation
               .dispatch(NavigationActions.reset(
                 {
                    index: 0,
                    actions: [
                      NavigationActions.navigate({ routeName: 'Grocery'})
                    ]
                  }));
  }

  reset(){

    this.shutdownOrderScreen();
  }

  constructor(props){
    super(props);

    this.state = {
      timerMinutes: '90',
      timerSeconds: '01',
    }

    this.reset = this.reset.bind(this);
    this.navigation = this.props.navigation;
    this.shutdownOrderScreen = this.shutdownOrderScreen.bind(this);
  }

  componentWillMount(){
    // Detect back key
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.shutdownOrderScreen();
    });
  }

  render() {

    return (

      <Container>
        <Header style={{backgroundColor: '#00BCD4', height: 65}}>
          <Body style={{height: '100%', alignItems: 'center', justifyContent: 'flex-end'}}>
            <Title style={{paddingBottom: 8, fontSize: 25}}>
              Pedido
            </Title>
          </Body>
        </Header>
        <Content contentContainerStyle={styles.viewContainer}>
          <View style={styles.viewContainer}>
            <Text style={{fontSize: 22}}>
              Tu pedido va a llegar antes
            </Text>
            <Text style={{fontSize: 22}}>
              que el reloj termine.
            </Text>
            <Text style={{fontSize: 30, marginTop: 10}}>
              {this.state.timerMinutes}:{this.state.timerSeconds}
            </Text>
          </View>
          <View style={styles.viewContainer}>
            <Text style={{fontSize: 22}}>
              Delivery Man:
            </Text>
            <View style={{flexDirection: 'row', alignItems: 'center', height: 120, width: 160, justifyContent: 'space-between' }}>
              <Text style={{fontSize: 20}}>
                Michael
              </Text>
              <Image source={require('../assets/images/michael.jpeg')} style={styles.deliveryManImage} resizeMode='contain' />
            </View>
          </View>
        </Content>
        <Footer style={{backgroundColor: '#FFFFFF', height: 60, justifyContent: 'center'}}>
          <Button iconRight transparent style={{flex: 1, height: '100%', justifyContent: 'center', alignItems: 'center', backgroundColor: '#00A823'}} onPress={() => this.reset()}>
            <Text style={{fontSize: 24, color: '#EEEEEE'}}>Recibí el pedido</Text>
          </Button>
        </Footer>
      </Container>
    );
  }

  componentDidMount(){

    // Countdown for delivery
    this.intervalCountdown = setInterval(() => {
      seconds = parseInt(this.state.timerSeconds);
      seconds--;

      if(seconds < 0){
        seconds = 59;

        const minutes = (parseInt(this.state.timerMinutes)-1).toString()
        this.setState({timerSeconds: seconds.toString()});
        this.setState({timerMinutes: minutes});
      }else if(seconds < 10){
        this.setState({timerSeconds: ( '0' + seconds.toString())});
      }else{
        this.setState({timerSeconds: seconds.toString()});
      }
    }, 1000);
  }
}

const styles = StyleSheet.create({
  viewContainer: {
    flex: 1,
    width: '100%',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  deliveryManImage: {
    alignSelf: 'center',
    height: 80,
    width: 80,
    borderWidth: 1,
    borderRadius: 75
  },
});
